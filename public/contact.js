class ContactForm {

  constructor(container) {
    this.form = container.querySelector('form');
    this.form.addEventListener('submit', (e) => this.#post(e));

    // If set, grab subject from url query params.
    if (location && location.search) {
        const params = new URLSearchParams(location.search);
        const s = this.form.querySelector('#subject');
        if (s) s.value = params.get('q');
    }

    // Initialize captcha (if present in DOM)
    const hint = this.form.querySelector('.captcha-hint');

    if (hint) {
      const url = this.form.action;

      fetch(url + '?captcha')
      .then(response => response.json())
      .then(data => {
        hint.textContent = data.name;
        hint.href = data.url;
        const captcha = document.querySelector('#captcha');
        captcha.value = data.index;
      })
      .catch((error) => {
        info(false, 'An error occured (see console for details).');
        console.error(error);
      });
    }

    // Set time value.
    this.#time(Date.now());
  }

  #time(t) {
    const time = this.form.querySelector('#time');
    if (time) {
      time.value = t.toString();
    }
  }

  #post(event) {
    const button = this.form.querySelector('button[type="submit"]');

    // Prevent double send.
    button.disabled = true;

    // Prevent the default behaviour of the browser submitting the form.
    event.preventDefault();

    // Gets the element which the event handler was attached to.
    const form = event.currentTarget;

    // Takes the API URL from the form's "action" attribute.
    const url = form.action;

    const data = new URLSearchParams();

    for (const pair of new FormData(form)) {
      data.append(pair[0], pair[1]);
    }

    fetch(url, { method: 'POST', body: data })
    .then((response) => {
      if (!response.ok) {
        throw new Error('Response status code indicates error');
      }
      return response.json();
    })
    .then((response) => {
      if (response.status === 'success') {
        this.#info(response.status, 'Message was sent!');
        this.#time(0);
      } else {
        this.#info(response.status, response.error);

        // If status is 'warning', the message was saved and we don't
        // need to re-enbale the submit button.
        if (response.status === 'error') {
          button.disabled = false;
        }
      }
    })
    .catch((error) => {
      this.#info('error', 'An error occured (see console for details).');
      console.error(error);
    });
  }

  #info(status, message) {
    const el = this.form.querySelector('.message');
    el.textContent = message;
    el.classList.remove('success', 'warning', 'error');
    el.classList.add(status);
  }
}

window.addEventListener('DOMContentLoaded', () => {
  const container = document.querySelector('.contact-form');
  if (container) {
    new ContactForm(container);
  }
})
