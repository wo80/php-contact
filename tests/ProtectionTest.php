<?php

use PHPUnit\Framework\TestCase;

use PhpContact\Database;
use PhpContact\Protection;

final class ProtectionTest extends TestCase
{
	private $config;

	protected function setUp(): void
	{
		$this->config = [];
		$this->config['db_name'] = 'test_protection.db';
		$this->config['max_messages_per_day'] = 5;
		$this->config['max_messages_per_ip'] = 2;
	}

	protected function tearDown(): void
	{
		unlink($this->config['db_name']);
	}
	
	public function test_max_messages_per_day() : void
	{
		$db = new Database($this->config);
		$protection = new Protection($db, $this->config);

		$this->assertTrue($protection->send('1.1.1.1'));
		$this->assertTrue($protection->send('1.1.1.1'));
		$this->assertFalse($protection->send('1.1.1.1')); // max_messages_per_ip
		$this->assertTrue($protection->send('1.1.1.2'));
		$this->assertTrue($protection->send('1.1.1.3'));
		$this->assertTrue($protection->send('1.1.1.4'));
		$this->assertFalse($protection->send('1.1.1.5')); // max_messages_per_day
		$this->assertFalse($protection->send('1.1.1.1')); // ...

		$db->deleteIps();
	}
	
	public function test_auto_delete() : void
	{
		$db = new Database($this->config);
		$protection = new Protection($db, $this->config);

		$db->addIp('1.1.1.1', 0, date('c', mktime(0, 0, 0, 1, 1, 2024)));
		$db->addIp('1.1.1.1', 0, date('c', mktime(0, 0, 0, 1, 2, 2024)));
		$db->addIp('1.1.1.1', 1, date('c', mktime(0, 0, 0, 1, 3, 2024)));

		$this->assertEquals(2, count($db->getIps(5, 0)));
		$this->assertEquals(1, count($db->getIps(5, 1)));

		// This should delete old entries.
		$protection->send('1.1.1.1');

		$this->assertEquals(1, count($db->getIps(5, 0)));
		$this->assertEquals(0, count($db->getIps(5, 1)));

		$db->deleteIps();
	}
}
