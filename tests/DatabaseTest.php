<?php

use PHPUnit\Framework\TestCase;

use PhpContact\Database;

final class DatabaseTest extends TestCase
{
	private $config;

	protected function setUp(): void
	{
		$this->config = [];
		$this->config['db_name'] = 'test_database.db';
	}

	protected function tearDown(): void
	{
		unlink($this->config['db_name']);
	}

	public function test_add_delete_message() : void
	{
		$db = new Database($this->config);
		$db->addMessage('name 1', 'mail@test.de', 'subject 1', 'message 1');
		$db->addMessage('name 2', 'mail@test.de', 'subject 2', 'message 2');
		$db->addMessage('name 3', 'mail@test.de', 'subject 3', 'message 3');

		$a = $db->getMessages(false);
		$this->assertEquals(3, count($a));

		$db->deleteMessage(1);
		$db->deleteMessage(2);

		$a = $db->getMessages(true);
		$this->assertEquals(1, count($a));
	}
}
