<?php

use PHPUnit\Framework\TestCase;

use PhpContact\Spam;

final class SpamTest extends TestCase
{
	protected function setUp(): void
	{
	}

	public function test_ok() : void
	{
		$json = [];
		$this->assertTrue(Spam::check('ok', 'ok', 'ok', $json));
	}

	public function test_reject_email_1() : void
	{
		$json = [];
		$this->assertFalse(Spam::check('cc:', 'ok', 'ok', $json));
		$this->assertEquals('Message was rejected (reason: invalid content in email field [1])', $json['error']);
	}

	public function test_reject_subject_1() : void
	{
		$json = [];
		$this->assertFalse(Spam::check('ok', 'CC:', 'ok', $json));
		$this->assertEquals('Message was rejected (reason: invalid content in subject field [1])', $json['error']);
	}
		
	public function test_reject_email_2a() : void
	{
		$json = [];
		$this->assertFalse(Spam::check("%0D", 'ok', 'ok', $json));
		$this->assertEquals('Message was rejected (reason: invalid content in email field [2])', $json['error']);
	}
		
	public function test_reject_email_2b() : void
	{
		$json = [];
		$this->assertFalse(Spam::check("\r\n", 'ok', 'ok', $json));
		$this->assertEquals('Message was rejected (reason: invalid content in email field [2])', $json['error']);
	}
		
	public function test_reject_subject_2a() : void
	{
		$json = [];
		$this->assertFalse(Spam::check('ok', "%0D", 'ok', $json));
		$this->assertEquals('Message was rejected (reason: invalid content in subject field [2])', $json['error']);
	}
		
	public function test_reject_subject_2b() : void
	{
		$json = [];
		$this->assertFalse(Spam::check('ok', "\r\n", 'ok', $json));
		$this->assertEquals('Message was rejected (reason: invalid content in subject field [2])', $json['error']);
	}
		
	public function test_pass_email() : void
	{
		$json = [];
		$this->assertTrue(Spam::check('buy now', 'ok', 'ok', $json));
	}
		
	public function test_spam_subject() : void
	{
		$json = [];
		$this->assertFalse(Spam::check('ok', 'buy NOW', 'ok', $json));
		$this->assertEquals('Message was rejected (reason: bad word in subject field [buy NOW])', $json['error']);
	}
		
	public function test_spam_message() : void
	{
		$json = [];
		$this->assertFalse(Spam::check('ok', 'ok', 'BUY now', $json));
		$this->assertEquals('Message was rejected (reason: bad word in message field [BUY now])', $json['error']);
	}
		
	public function test_spam_message_normalized() : void
	{
		$json = [];
		$this->assertFalse(Spam::check('ok', 'ok', 'BÜy nów', $json));
		$this->assertEquals('Message was rejected (reason: bad word in message field [BUy now])', $json['error']);
	}
}
