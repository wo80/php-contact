<?php

use PHPUnit\Framework\TestCase;

use PhpContact\Database;
use PhpContact\Contact;

final class ContactTest extends TestCase
{
	private $config;
	private Contact $contact;

	protected function setUp(): void
	{
		$this->config = [];
		$this->config['db_name'] = 'test_contact.db';
		$this->config['mail_address'] = '';
		$this->config['max_content_length'] = 20;
		$this->config['host'] = 'server.com';

		$db = new Database($this->config);
		$this->contact = new Contact($db, $this->config);
	}

	protected function tearDown(): void
	{
		unlink($this->config['db_name']);
	}

	public function test_missing_name() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'subject' => '',
			'message' => 'test',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Missing field: name');
	}

	public function test_empty_name() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => '',
			'subject' => '',
			'message' => 'test',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Empty field: name');
	}

	public function test_missing_subject() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => 'test',
			'message' => 'test',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Missing field: subject');
	}

	public function test_empty_subject() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => 'test',
			'subject' => '',
			'message' => 'test',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Empty field: subject');
	}

	public function test_max_length_name() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => str_repeat('0123456789', 20),
			'subject' => 'test',
			'message' => 'test',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Field \'name\' exceeded max. allowed length (50).');
	}

	public function test_missing_message() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => 'test',
			'subject' => 'test',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Missing field: message');
	}

	public function test_empty_message() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => 'test',
			'subject' => 'test',
			'message' => '',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Empty field: message');
	}

	public function test_max_length_message() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => 'test',
			'subject' => 'test',
			'message' => str_repeat('0123456789', 5),
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Field \'message\' exceeded max. allowed length.');
	}

	public function test_invalid_mail_address() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test',
			'name' => 'test',
			'subject' => 'test',
			'message' =>
			'test',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Invalid e-mail address.');
	}

	public function test_reject_honeypot() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => 'test',
			'subject' => 'test',
			'message' => 'test',
			'phone' => '0',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Message was rejected (reason: honeypot)');
	}

	public function test_reject_instant_submit() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => 'test',
			'subject' => 'test',
			'message' => 'test',
			'time' => ''.((time() - 14) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Message was rejected (reason: instant submit)');
	}

	public function test_reject_expired() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => 'test',
			'subject' => 'test',
			'message' => 'test',
			'time' => ''.((time() - 10001) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Message was rejected (reason: expired)');
	}

	public function test_referer() : void
	{
		$_SERVER = ['HTTP_REFERER' => 'http://hacker.com'];
		$_POST = [
			'email' => 'test@example.com',
			'name' => 'test',
			'subject' => 'test',
			'message' => 'test',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['error'], 'Invalid HTTP referer.');
	}

	public function test_ok() : void
	{
		$_SERVER = ['HTTP_REFERER' => $this->config['host']];
		$_POST = [
			'email' => 'test@example.com',
			'name' => 'test',
			'subject' => 'test',
			'message' => 'test',
			'time' => ''.((time() - 20) * 1000)
		];

		$json = $this->contact->send();
		$this->assertEquals($json['status'], 'success');
	}
}
