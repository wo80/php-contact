# PHP Contact Form

A simple, dependency-free PHP contact form.

## Features

* Easy to use, minimal configuration.
* Fully functional HTML example form included.
* Protection against misuse:
   * Time-based protection (no submit accepted before 15 seconds or after one hour).
   * Honeypot form field `phone`.
   * _Bad words_ filter (not yet configurable - see `Spam.php`).
   * Optional captcha (hand-made, no annoying reCAPTCHA support).
   * Only a limited number of messages can be sent per day (default = 50).
   * Only a limited number of messages can be sent from the same IP address per day (default = 5).
   * Mails are always sent as `text/plain` - no HTML mails.
* Store messages in a database in case the PHP `mail()` function is not available or unreliable.

## Requirements

* PHP version 8.1 or higher.
* The PHP `mail()` function must be configured to send the messages (done by your hosting provider).
* The `php_pdo_sqlite` extension must be enabled in case the database should be used.
* The `php_openssl` extension must be enabled for encryption of database content.

## Data Privacy

* In case messages are stored in a database, the content of `email` and `message` fields are encrypted.
* Messages are deleted from the database after 30 days.
* The `email` field of the HTML form is optional by default.
* For protection against DoS attacks, hashes of IPs are stored.
* IP hashes are deleted from the database after 2 days.
* Note that the messages send via PHP `mail()` are not encrypted.

## Configuration

All configuration is done in the `config.php` file. The options are documented in the comments.

The following options MUST be changed for a secure configuration of the SQLite database:

* `db_name`: Change the default SQLite database name. Otherwise, an attacker can just download the file.
* `password`: Change the default password to access the messages in the database.
* `encryption_key`: Change the default encryption key.

The following options CAN be changed for enhanced security of the form:

* `host`: Enter your host name to enable the `HTTP_REFERER` check.

To use a standard SQL database server instead of SQLite, you will have to edit the `open()` method in `Database.php`. Remove the SQLite part and use the commented MySQL code as a blueprint.

To enable captchas, set the config option `captcha_required` to true and uncomment the captcha fields in the `contact.html` example.

## Examples

A fully functional HTML form is available in the [public](https://gitlab.com/wo80/php-contact/-/tree/main/public) folder. Just start a PHP test server in the root folder of the project
```shell
php -S localhost:8000
```
and then open http://localhost:8000/public/contact.html .

You can access the messages in the database using `curl`:
```shell
curl -d "password=hello" -X POST http://localhost:8000/src/api.php?view
```
Use [jq](https://github.com/jqlang/jq) to convert the response into a better readable format:
```shell
curl -d "password=hello" -X POST http://localhost:8000/src/api.php?view | jq .
```
## Tests

To run the unit tests, install [PHPUnit](https://phpunit.de/index.html) and run
```shell
./phpunit --bootstrap src/autoload.php tests
```
Static analysis has been done with [PHPStan](https://phpstan.org/)
```shell
./phpstan analyse src --level 1
```
and [Psalm](https://psalm.dev/) (configuration in `psalm.xml`)
```shell
./psalm
```
