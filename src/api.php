<?php

require_once 'autoload.php';
require_once 'config.php';

use PhpContact\Database;
use PhpContact\Contact;

header("Content-Type: application/json; charset=UTF-8");

$db = new Database($config);
$contact = new Contact($db, $config);

if (isset($_POST['name'])) {
	echo json_encode($contact->send());
} elseif (isset($_GET['view'])) {
	$response = $contact->view();
	http_response_code($response['code']);
	echo json_encode($response);
} elseif (isset($_GET['prune'])) {
	$response = $contact->prune();
	http_response_code($response['code']);
	echo json_encode($response);
} elseif (isset($_GET['captcha'])) {
	echo json_encode(Contact::getCaptcha());
} else {
	echo json_encode(array('error' => 'Invalid API call.'));
}
