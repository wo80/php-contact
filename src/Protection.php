<?php
declare(strict_types=1);

namespace PhpContact;

use PhpContact\Database;

//require_once 'Database.php';

class Protection
{
	/** The message database. */
	public Database $db;

	/** @var int The maximum number of allowed messages per day */
	private int $maxPerDay;

	/** @var int The maximum number of allowed messages per day and ip */
	private int $maxPerIp;
	
	private string $last_error;

	public function __construct()
	{
		if (func_num_args() !== 2) {
			throw new \Exception('Invalid number of constructor arguments.');
		}

		$this->db = func_get_arg(0);
		$c = func_get_arg(1);

		if (!is_array($c)) {
			throw new \Exception('Invalid constructor argument (expexted array).');
		}

		$this->maxPerDay = $c['max_messages_per_day'] ?? 50;
		$this->maxPerIp = $c['max_messages_per_ip'] ?? 5;
	}

	/**
	 * Check whether the IP has recently tried to access a restricted API endpoint.
	 */
	public function getLastError(): string
	{
		$e = $this->last_error;
		$this->last_error = '';
		return $e;
	}

	/**
	 * Check whether the IP has recently sent messages.
	 */
	public function send(string $ip = null): bool
	{
		return $this->check(0, $ip);
	}

	/**
	 * Check whether the IP has recently tried to access a restricted API endpoint.
	 */
	public function login(string $ip = null): bool
	{
		return $this->check(1, $ip);
	}

	private function check(int $type, string $ip = null): bool
	{
		try {
			// Delete older entries.
			$this->db->deleteIps();
			
			$a = $this->db->getIps($this->maxPerDay, $type);
			$n = count($a);

			$ip ??= $this->getIp();

			$now = (new \DateTime('now'))->getTimestamp();

			if ($n === $this->maxPerDay) {
				$last = $a[$n - 1];

				$end = (new \DateTime($last['date']))->getTimestamp();

				$hours = ($now - $end) / 60 / 60;

				if ($hours < 24) {
					// We had more messages than allowed in the last 24 hours.
					return false;
				}
			}

			if ($this->countIp($ip, $now, $a) >= $this->maxPerIp) {
				// We had more messages for given IP than allowed in the last 24 hours.
				return false;
			}

			$this->db->addIp($ip, $type);

			return true;
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * Count the number of IP addresses in last 24 hours.
	 */
	private function countIp(string $ip, int $now, array $data): int
	{
		$k = 0;

		foreach ($data as $e) {
			$time = (new \DateTime($e['date']))->getTimestamp();

			$hours = ($now - $time) / 60 / 60;
			
			if ($hours > 24) break;

			if ($e['ip'] === $ip) $k++;
		}

		return $k;
	}

	/**
	 * Get the hashed IP address.
	 */
	private function getIp(): string
	{
		$ip = $_SERVER['REMOTE_ADDR'] ?? '';

		if (empty($ip)) {
			$test = ['REMOTE_ADDR', 'HTTP_X_FORWARDED_FOR', 'HTTP_CLIENT_IP'];

			foreach ($test as $i) {
				$ip = getenv($i, true) ?: getenv($i);

				if (!empty($ip)) break;
			}

			$ip = 'unknown';
		}

		return $this->getHash($ip);
	}

	private function getHash(string $ip): string
	{
		return hash('xxh3', $ip); // fast 64-bit hash, requires PHP >= 8.1
	}
}
