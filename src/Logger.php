<?php
declare(strict_types=1);

namespace PhpContact;

class Logger
{
	public static function info(string $method, string $message): void
	{
		self::write('[I]', $method . ': ' . $message);
	}

	public static function warn(string $method, string $message): void
	{
		self::write('[W]', $method . ': ' . $message);
	}

	public static function error(string $method, string $message, array &$json = null): void
	{
		self::write('[E]', $method . ': ' . $message);
	}

	private static function write(string $level, string $message): void
	{
		file_put_contents('contact.log', date('Y-m-d H:i:s').' '.$level.' '.$message.PHP_EOL, FILE_APPEND);
	}
}
