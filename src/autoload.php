<?php

declare(strict_types=1);

function contact_autoload(string $class): void
{
	if (false === strpos($class, 'PhpContact\\')) {
		return;
	}

	$namespaceMap = ['PhpContact\\' => __DIR__ . '/'];

	foreach ($namespaceMap as $prefix => $dir) {
		// swap out the namespace prefix with a directory
		$path = str_replace($prefix, $dir, $class);
		// replace the namespace separator and add the PHP file extension
		$path = str_replace('\\', '/', $path) . '.php';

		// uncomment for debugging
		//if (!file_exists($path)) echo 'Not found: ' . $path . PHP_EOL;

		@include_once $path;
	}
}

spl_autoload_register('contact_autoload');
