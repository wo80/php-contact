<?php

$config = array(
	/* The database name. */
	'db_name' => 'contact.db',

	/* The database host (not needed for SQLite). */
	'db_host' => '',

	/* The database user name (not needed for SQLite). */
	'db_user' => '',

	/* The database password (not needed for SQLite). */
	'db_pass' => '',

	/* The mail address to send notifications to (leave empty if no notification is wanted). */
	'mail_address' => '',

	/* If true, the message body will be included in notification mails. */
	'include_message' => true,

	/* If true, the a captcha must be solved. */
	'captcha_required' => true,

	/* The maximum allowed content length. */
	'max_content_length' => 2000,

	/* The host name to check the HTTP_REFERER for (leave empty to skip test). */
	'host' => '',

	/* The password to view messages. */
	'password' => 'hello',

	/* The key used to encrypt data in the database. */
	'encryption_key' => 'GGD6zhTS+hgSH6!dfhs',

	/* Max. messages per day. */
	'max_messages_per_day' => 50,

	/* Max. messages per IP per day. */
	'max_messages_per_ip' => 5
);
