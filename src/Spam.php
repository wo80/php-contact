<?php
declare(strict_types=1);

namespace PhpContact;

class Spam
{
	/**
	 * Check form data for spam.
	 *
	 * @param string $email   The email.
	 * @param string $subject The subject.
	 * @param string $message The message.
	 * @param array  $json    The json array containing the error message.
	 *
	 * @returns bool Returns true if all checks passed, otherwise false.
	 */
	public static function check(string $email, string $subject, string $message, array &$json): bool
	{
		if (!self::checkContent($email, "email", $json) ||
			!self::checkContent($subject, "subject", $json) ||
			!self::checkBadWords($subject, "subject", $json) ||
			!self::checkBadWords($message, "message", $json)) {
			return false;
		}

		return true;
	}
	
	private static function checkContent(string $s, string $name, array &$json): bool
	{
		if (preg_match("/(to:|cc:|bcc:|from:|subject:|reply-to:|content-type:|mime-version:|multipart\/mixed|content-transfer-encoding:)/i", $s) === 1) {
			$json['error'] = 'Message was rejected (reason: invalid content in '.$name.' field [1])';
			return false;
		}

		if (preg_match("/%0A|\\r|%0D|\\n|%00|\\0|%09|\\t|%01|%02|%03|%04|%05|%06|%07|%08|%09|%0B|%0C|%0E|%0F|%10|%11|%12|%13/i", $s) === 1) {
			$json['error'] = 'Message was rejected (reason: invalid content in '.$name.' field [2])';
			return false;
		}

		return true;
	}

	private static function checkBadWords(string $s, string $name, array &$json): bool
	{
		$BADWORDS = ['order now', 'buy now', 'credit card'];
		
		if (preg_match("/(".implode('|', $BADWORDS).")/i", self::normalize($s), $matches) === 1) {
			$json['error'] = 'Message was rejected (reason: bad word in '.$name.' field ['.$matches[0].'])';
			return false;
		}

		return true;
	}

	private static function normalize(string $s): string|array|null
	{
		$s = preg_replace('/&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);/i', '$1', htmlentities($s, ENT_QUOTES, 'UTF-8'));
		
		return preg_replace('/[\x{0300}-\x{036f}]/u', '', $s);
	}
}
