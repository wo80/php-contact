<?php
declare(strict_types=1);

namespace PhpContact;

use PhpContact\Database;
use PhpContact\Captcha;
use PhpContact\Logger;
use PhpContact\Protection;
use PhpContact\Spam;

class Contact
{
	/** The maximum number of characters allowed in the 'name' field. */
	const MAX_NAME_LENGTH = 50;
	
	/** The maximum number of characters allowed in the 'subject' field. */
	const MAX_SUBJECT_LENGTH = 200;
	
	/** The mail address (empty, if no notification wanted). */
	private string $mailAddress;

	/** The password to access the message view. */
	private string $password;

	/** The host (empty, if no check wanted). */
	private string $host;

	/** The messages database. */
	private Database $db;

	/** The DoS protection. */
	private Protection $protection;

	/** Is a sender email address required? */
	private bool $mailRequired = false;

	/** Is a captcha required? */
	private bool $captchaRequired;

	/** Include message content in mail? */
	private bool $includeMessage;

	/** The maximum number of characters allowed in the message body. */
	private int $maxContentLength;

	/** Automatically delete messages after a given number of days. */
	private int $daysToKeepMessages = 30;

	/** Use quoted-printable as content-transfer-encoding? */
	private bool $qprint = false;

	public function __construct()
	{
		if (func_num_args() !== 2) {
			throw new \Exception('Invalid number of constructor arguments.');
		}

		$this->db = func_get_arg(0);
		$c = func_get_arg(1);

		if (!is_array($c)) {
			throw new \Exception('Invalid constructor argument (expexted array).');
		}

		$this->mailAddress = $c['mail_address'] ?? '';
		$this->password = $c['password'] ?? '';
		$this->host = $c['host'] ?? '';
		$this->includeMessage = $c['include_message'] ?? false;
		$this->captchaRequired = $c['captcha_required'] ?? false;
		$this->maxContentLength = $c['max_content_length'] ?? 2000;
		
		$this->protection = new Protection($this->db, $c);
	}

	/**
	 * Undocumented function
	 *
	 * @return array
	 */
	public static function getCaptcha() : array
	{
		return (new Captcha())->getCaptcha();
	}

	/**
	 * Return messages from the database.
	 *
	 * @return array
	 */
	public function view() : array
	{
		$json = array('status' => 'error', 'code' => 400);

		if (!isset($_POST['password']) || strcmp($this->password, $_POST['password']) !== 0) {
			Logger::warn('Contact.view', 'invalid password');

			$json['error'] = 'Invalid password';
			$json['code'] = 401;
			return $json;
		}

		if (!$this->protection->login()) {
			Logger::warn('Contact.view', 'DoS protection');

			$json['error'] = 'DoS protection is active';
			$json['code'] = 429;
			return $json;
		}

		try {
			$json['items'] = $this->db->getMessages(true);
			$json['status'] = 'success';
			$json['code'] = 200;
		} catch (\Exception $e) {
			Logger::error('Contact.view', $e->getMessage());
			$json['error'] = $e->getMessage();
		}

		return $json;
	}

	/**
	 * Remove old messages and ips from the database.
	 *
	 * @return array
	 */
	public function prune() : array
	{
		$json = array('status' => 'error', 'code' => 400);

		if (!isset($_POST['password']) || strcmp($this->password, $_POST['password']) !== 0) {
			Logger::warn('Contact.prune', 'invalid password');

			$json['error'] = 'Invalid password';
			$json['code'] = 401;
			return $json;
		}

		try {
			$this->db->deleteMessages($this->daysToKeepMessages);
			$this->db->deleteIps(2);
			$json['status'] = 'success';
			$json['code'] = 200;
		} catch (\Exception $e) {
			Logger::error('Contact.prune', $e->getMessage());
			$json['error'] = $e->getMessage();
		}

		return $json;
	}

	/**
	 * Send contact form data via mail and/or store content in the database.
	 *
	 * @return array
	 */
	public function send() : array
	{
		$json = array('status' => 'error');

		if (!$this->validatePostData($json)) {
			return $json;
		}

		if (!$this->validateCaptcha($json)) {
			return $json;
		}

		if (!$this->validateReferer($json)) {
			return $json;
		}

		if (!empty($_POST['phone']) || (isset($_POST['phone']) && strlen($_POST['phone']) > 0)) {
			Logger::warn('Contact.send', 'message rejected (reason: honeypot)');

			$json['error'] = 'Message was rejected (reason: honeypot)';
			return $json;
		}

		if (empty($_POST['time'])) {
			Logger::warn('Contact.send', 'message rejected (reason: missing timestamp)');

			$json['error'] = 'Message was rejected (reason: missing timestamp)';
			return $json;
		}

		// Time difference in seconds.
		$diff = (time() * 1000 - intval($_POST['time'])) / 1000;
		
		if ($diff < 15) {
			Logger::warn('Contact.send', 'message rejected (reason: instant submit)');

			$json['error'] = 'Message was rejected (reason: instant submit)';
			return $json;
		}

		if ($diff > 3600) {
			Logger::warn('Contact.send', 'message rejected (reason: expired)');

			$json['error'] = 'Message was rejected (reason: expired)';
			return $json;
		}

		$name = strval($_POST['name']);
		$email = strval($_POST['email']);
		$subject = strval($_POST['subject']);
		$message = strval($_POST['message']);

		if (!Spam::check($email, $subject, $message, $json)) {
			Logger::warn('Contact.send', 'message rejected (reason: spam detected)');

			$json['error'] = 'Message was rejected (reason: spam detected)';
			return $json;
		}

		if (!$this->protection->send()) {
			Logger::warn('Contact.send', 'message rejected (reason: DoS protection)');

			$json['error'] = 'Message was rejected (reason: DoS protection is active - try again in 24 hours)';
			return $json;
		}

		$saveStatus = $this->db->ready();

		if ($saveStatus) {
			$saveStatus = $this->save($name, $email, $subject, $message, $json);
		}

		$success = $this->sendMail($name, $email, $subject, $message, $json);

		if ($success) {
			$json['status'] = 'success';
		} elseif ($saveStatus) {
			$json['status'] = 'warning';
			$json['error'] .= ', but message was successfully saved to database';
		} else {
			$json['status'] = 'error';
		}

		return $json;
	}

	/** Save message to database. */
	private function save(string $name, string $email, string $subject, string $message, array &$json): bool
	{
		try {
			$this->db->deleteMessages($this->daysToKeepMessages);

			$this->db->addMessage($name, $email, htmlspecialchars($subject), htmlspecialchars($message));

			Logger::info('Contact.save', 'message saved to database');

			return true;
		} catch (\Exception $e) {
			Logger::error('Contact.save', $e->getMessage());

			$json['error'] = $e->getMessage();
		}

		return false;
	}

	/** Send message via mail. */
	private function sendMail(string $name, string $email, string $subject, string $message, array &$json): bool
	{
		try {
			$to = $this->mailAddress;

			if (empty($to)) {
				return true;
			}

			if (filter_var($to, FILTER_VALIDATE_EMAIL) === false) {
				$json['error'] = 'Invalid configuration (mail_address)';
				return false;
			}

			$sender = empty($email) ? 'no-reply@nirvana.space' : $email;

			$headers = [];
			$headers[] = 'From: "'.$name.' via PHP Contact" <'.$sender.'>';
			if (!empty($email)) {
				$headers[] = 'Reply-To: '.$email;
			}
			$headers[] = 'Content-type: text/plain; charset=utf-8';

			if ($this->includeMessage) {
				$content = $message;
			} else {
				$content = 'New message arrived (content length: '.strlen($message).').';
			}

			$subject = '[PHP Contact] '.$subject;

			if (preg_match('/[^\x20-\x7e]/', $subject)) {
				$subject = '=?UTF-8?B?'.base64_encode($subject).'?=';
			}

			if (preg_match('/[^\x20-\x7e]/', $message)) {
				// If message contains non-ASCII characters, send content as
				// quoted-printable or base64.

				if ($this->qprint) {
					// https://www.php.net/manual/en/function.quoted-printable-encode.php
					$headers[] = 'Content-Transfer-Encoding: quoted-printable';
					$content = quoted_printable_decode($content);
				} else {
					// https://www.php.net/manual/en/function.mail.php#124291
					$headers[] = 'Content-Transfer-Encoding: base64';
					$content = base64_encode($content);
				}
			}

			$headers[] = 'X-Mailer: PHP/'.phpversion();

			// Try to send the mail.
			if (@mail($to, $subject, $content, implode("\r\n", $headers))) {
				return true;
			}

			$json['error'] = 'PHP mail() function failed';

			Logger::error('Contact.sendMail', 'PHP mail() function failed');
		} catch (\Exception $e) {
			$json['error'] = $e->getMessage();

			Logger::error('Contact.sendMail', $e->getMessage());
		}

		return false;
	}

	private function validatePostData(array &$json) : bool
	{
		$required = ['name', 'subject', 'message'];

		if ($this->mailRequired) {
			$required[] = 'email';
		}

		if ($this->captchaRequired) {
			$required[] = 'captcha';
			$required[] = 'answer';
		}

		foreach ($required as $name) {
			if (!isset($_POST[$name])) {
				$json['error'] = 'Missing field: '.$name;
				return false;
			}

			if (empty($_POST[$name])) {
				$json['error'] = 'Empty field: '.$name;
				return false;
			}
		}

		if (strlen($_POST['name']) > self::MAX_NAME_LENGTH) {
			$json['error'] = 'Field \'name\' exceeded max. allowed length ('.self::MAX_NAME_LENGTH.').';
			return false;
		}

		if (strlen($_POST['subject']) > self::MAX_SUBJECT_LENGTH) {
			$json['error'] = 'Field \'subject\' exceeded max. allowed length ('.self::MAX_SUBJECT_LENGTH.').';
			return false;
		}

		if (strlen($_POST['message']) > $this->maxContentLength) {
			$json['error'] = 'Field \'message\' exceeded max. allowed length.';
			return false;
		}

		if (!empty($_POST['email']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
			$json['error'] = 'Invalid e-mail address.';
			return false;
		}

		return true;
	}

	private function validateReferer(array &$json) : bool
	{
		if (empty($this->host)) {
			return true;
		}

		if (!isset($_SERVER['HTTP_REFERER'])) {
			return true;
		}
		
		$referer = parse_url($_SERVER['HTTP_REFERER']);

		if ($referer === false) {
			return true;
		}
		
		if (!array_key_exists('scheme', $referer)) {
			$referer = parse_url('http://' . $_SERVER['HTTP_REFERER']);
		}

		if ($referer === false || !array_key_exists('host', $referer)) {
			return true;
		}

		if (!str_contains($referer['host'], $this->host)) {
			$json['error'] = 'Invalid HTTP referer.';
			return false;
		}

		return true;
	}

	private function validateCaptcha(array &$json) : bool
	{
		if (!$this->captchaRequired) {
			return true;
		}

		$c = new Captcha();

		$i = intval($_POST['captcha']);
		$answer = strval($_POST['answer']);

		if ($c->checkCaptcha($i, $answer) === false) {
			$json['error'] = 'Captcha wrong answer.';
			return false;
		}

		return true;
	}
}
