<?php
declare(strict_types=1);

namespace PhpContact;

use PDO;
use PDOException;

class Database
{
	/** @var string Database host (not needed for SQLite) */
	private $db_host;

	/** @var string Database user (not needed for SQLite) */
	private $db_user;

	/** @var string Database password (not needed for SQLite) */
	private $db_pass;

	/** @var string Database name (for SQLite the file name) */
	private $db_name;

	/** @var string Encryption key. */
	private $key;

	public function __construct()
	{
		if (func_num_args() !== 1) {
			throw new \Exception('Invalid number of constructor arguments.');
		}

		$c = func_get_arg(0);

		if (!is_array($c)) {
			throw new \Exception('Invalid constructor argument (expexted array).');
		}

		$this->db_host = $c['db_host'] ?? '';
		$this->db_user = $c['db_user'] ?? '';
		$this->db_pass = $c['db_pass'] ?? '';
		$this->db_name = $c['db_name'] ?? '';

		$this->key = $c['encryption_key'] ?? '';

		if (!empty($this->db_name) && !file_exists($this->db_name)) {
			$this->createTables();
		}
	}

	/**
	 * Check whether the database is ready.
	 *
	 */
	public function ready(): bool
	{
		return !empty($this->db_name);
	}

	/**
	 * Add a message to the database.
	 *
	 * @param string $name     The name of the sender.
	 * @param string $mail     The mail of the sender.
	 * @param string $subject  The subject of the message.
	 * @param string $message  The message content.
	 *
	 */
	public function addMessage(string $name, string $mail, string $subject, string $message): void
	{
		try {
			$sql = 'INSERT INTO messages (date, name, mail, subject, message, length) VALUES(:date, :name, :mail, :subject, :message, :length);';

			$db = $this->open();

			$s = $db->prepare($sql);

			$date = date('c');
			$length = strlen($message);
			$mail_crypt = $this->encrypt($mail);
			$message_crypt = $this->encrypt($message);

			$s->bindParam(':date', $date);
			$s->bindParam(':name', $name);
			$s->bindParam(':mail', $mail_crypt);
			$s->bindParam(':subject', $subject);
			$s->bindParam(':message', $message_crypt);
			$s->bindParam(':length', $length);

			$s->execute();
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * Get messages.
	 *
	 * @param bool   $full      If true, include message content.
	 * @param string $sinceDate Return messages only since given date.
	 *
	 * @return array
	 */
	public function getMessages(bool $full, string $sinceDate = '') : array
	{
		try {
			$db = $this->open();

			$query = 'SELECT '.($full ? '*' : 'id, date, name, subject, length').' FROM messages';

			if (!empty($sinceDate)) {
				$query = $query.' WHERE date > :date';
			}

			$query = $query.' ORDER BY date;';

			$s = $db->prepare($query);
			
			if (!empty($sinceDate)) {
				$s->bindParam(':date', $sinceDate);
			}

			$s->execute();

			$result = $s->fetchAll(PDO::FETCH_ASSOC);

			if ($full) {
				foreach ($result as &$item) {
					if (!empty($item['mail'])) {
						$item['mail'] = $this->decrypt($item['mail']);
					}
					if (!empty($item['message'])) {
						$item['message'] = $this->decrypt($item['message']);
					}
				}
			}

			return $result;
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * Get the message with given id.
	 *
	 * @param int $id  The message id.
	 *
	 * @return array
	 */
	public function getMessage(int $id) : array
	{
		try {
			$db = $this->open();

			$s = $db->prepare('SELECT * FROM messages WHERE id = :id;');
			$s->bindParam(':id', $id, PDO::PARAM_INT);
			$s->execute();

			$m = $s->fetch(PDO::FETCH_ASSOC);

			return array(
				'date' => $m['date'],
				'name' => $m['name'],
				'mail' => $this->decrypt($m['mail']),
				'subject' => $m['subject'],
				'message' => $this->decrypt($m['message']),
				'length' => $m['length']
			);
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * Delete message with given id.
	 *
	 * @param int $id  The message id.
	 */
	public function deleteMessage(int $id): void
	{
		try {
			$db = $this->open();

			$s = $db->prepare('DELETE FROM messages WHERE id = :id;');
			$s->bindParam(':id', $id, PDO::PARAM_INT);
			$s->execute();
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * Delete all message older than given number of days.
	 *
	 * @param int $days  The number of days to keep (0 = default, means delete all messages).
	 */
	public function deleteMessages(int $days = 0): void
	{
		try {
			$db = $this->open();
			
			$sql = 'DELETE FROM messages';
			
			if ($days > 0) {
				$sql .= " WHERE date <= date('now','-" . $days . " days');";
			}

			$s = $db->prepare($sql);
			$s->execute();
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * Get a limited number of ips with given type (ordered date, newest fist).
	 *
	 * @param int $limit  The number of ips to return.
	 * @param int $type   The type (protection of 'send' [type = 0] or 'login' [type = 1] endpoints).
	 *
	 * @return array
	 */
	public function getIps(int $limit, int $type) : array
	{
		try {
			$db = $this->open();

			$s = $db->prepare('SELECT * FROM protection WHERE type = :type ORDER BY date DESC LIMIT '.$limit.';');
			$s->bindParam(':type', $type);
			$s->execute();
			return $s->fetchAll(PDO::FETCH_ASSOC);
		} catch (\Exception $e) {
			throw $e;
		}
	}
	
	/**
	 * Add ip given type.
	 *
	 * @param string $ip   The (hashed) ip to add.
	 * @param int    $type The type (protection of 'send' [type = 0] or 'login' [type = 1] endpoints).
	 * @param string $date The date.
	 */
	public function addIp(string $ip, int $type, string $date = null): void
	{
		try {
			$db = $this->open();

			$date ??= date('c');

			$s = $db->prepare('INSERT INTO protection (ip, date, type) VALUES(:ip, :date, :type);');
			$s->bindParam(':ip', $ip);
			$s->bindParam(':date', $date);
			$s->bindParam(':type', $type);
			$s->execute();
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * Delete all ips older than given number of days.
	 *
	 * @param int $days  The number of days to keep (default = 2).
	 */
	public function deleteIps(int $days = 2): void
	{
		try {
			$db = $this->open();
			
			$s = $db->prepare("DELETE FROM protection WHERE date <= date('now','-" . $days . " days');");
			$s->execute();
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * Delete all message older than given number of days.
	 */
	public function deleteMessagesm(): void
	{
	}

	private function open() : PDO
	{
		// Open database
		try {
			// For MySQL, use the following:
			/*
			$dsn = 'mysql:host='. $this->db_host .';dbname='. $this->db_name .';port=3306;charset=utf8mb4';
			$options = [
				PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
				PDO::ATTR_EMULATE_PREPARES   => false,
			];
			$db = new PDO($dsn, $this->db_user, $this->db_pass, $options);
			//*/
			
			$db = new PDO('sqlite:' . $this->db_name);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $db;
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/** Create database tables. */
	private function createTables(): void
	{
		try {
			$db = $this->open();

			$db->exec('BEGIN');

			// Messages table.
			$db->exec(
				'CREATE TABLE IF NOT EXISTS messages (
	id INTEGER PRIMARY KEY AUTOINCREMENT ,
	date DATETIME ,
	name TEXT ,
	mail TEXT ,
	subject TEXT ,
	message TEXT ,
	length INTEGER ,
	flag INTEGER DEFAULT 0
);'
			);

			// Ip table.
			$db->exec(
				'CREATE TABLE IF NOT EXISTS protection (
	id INTEGER PRIMARY KEY AUTOINCREMENT ,
	ip TEXT ,
	date DATETIME ,
	type INTEGER DEFAULT 0
);'
			);

			$db->exec('COMMIT');
		} catch (\Exception $e) {
			echo $e->getMessage();
		}
	}

	private function encrypt(string $data) : string
	{
		if (empty($data)) {
			return '';
		}
		$key = $this->key;
		$plaintext = $data;
		$ivlen = openssl_cipher_iv_length($cipher = 'AES-128-CBC');
		$iv = openssl_random_pseudo_bytes($ivlen);
		$ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
		$hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);
		$ciphertext = base64_encode($iv . $hmac . $ciphertext_raw);
		return $ciphertext;
	}

	private function decrypt(string $data) : string
	{
		if (empty($data)) {
			return '';
		}
		$key = $this->key;
		$c = base64_decode($data);
		$ivlen = openssl_cipher_iv_length($cipher = 'AES-128-CBC');
		$iv = substr($c, 0, $ivlen);
		$hmac = substr($c, $ivlen, $sha2len = 32);
		$ciphertext_raw = substr($c, $ivlen + $sha2len);
		$plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
		$calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);
		if (hash_equals($hmac, $calcmac)) {
			return $plaintext;
		}
		throw new \Exception('decryption failed');
	}
}
