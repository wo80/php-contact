<?php
declare(strict_types=1);

namespace PhpContact;

class Captcha
{
	private array $captchas;

	public function __construct()
	{
		$c = array();

		$c[] = [ '1879', 'Albert Einstein' ];
		$c[] = [ '1918', 'Richard Feynman' ];
		$c[] = [ '1912', 'Alan Turing' ];
		$c[] = [ '1777', 'Carl Friedrich Gauss' ];
		$c[] = [ '1707', 'Leonhard Euler' ];
		$c[] = [ '1685', 'Johann Sebastian Bach' ];
		$c[] = [ '1756', 'Wolfgang Amadeus Mozart' ];
		$c[] = [ '1724', 'Immanuel Kant' ];
		$c[] = [ '1926', 'Miles Davis' ];
		$c[] = [ '1926', 'John Coltrane' ];
		$c[] = [ '1915', 'Billie Holiday' ];
		$c[] = [ '1942', 'Jimi Hendrix' ];
		$c[] = [ '1931', 'James Dean' ];
		$c[] = [ '1926', 'Marilyn Monroe' ];
		$c[] = [ '1942', 'Muhammad Ali' ];

		$this->captchas = $c;
	}

	public function getCaptcha(): array
	{
		$i = random_int(0, count($this->captchas) - 1);
		$name = $this->captchas[$i][1];
		$url = 'https://en.wikipedia.org/wiki/'.str_replace(' ', '_', $name);
		
		return array('index' => $i, 'name' => $name, 'url' => $url);
	}

	public function checkCaptcha(int $i, string $answer): bool
	{
		return $this->captchas[$i][0] === $answer;
	}
}
